<?php
//Add
function insertTodo(){
    if(!empty($_GET['item'])){
        $insert = new insert ($_GET['item']);
        if($insert->insertTask()){
            echo'<div class="alert alert-success alert-dismissible fade show" role="alert">
            <p>Data Added Succesfully!</p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
             </div>
             ';
        }else{
            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
            <p>Data Failed to Add</p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>';
        }
    }
}

//Delete
function deleteTodo(){
    if(!empty($_GET['delete'])){
        $delete = new delete ($_GET['delete']);
        if($delete->deleteTask()){
            echo'<div class="alert alert-danger alert-dismissible fade show" role="alert">
            <p>Data Succesfully Deleted!</p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
             </div>
             ';
        }else{
            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
            <p>Data Failed to Delete</p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>';
        }
    }
}

//Edit
function editTodo(){
    if(!empty($_GET['edit'])){
        $edit = new edit ($_GET['edit']);
        if($edit->editTask()){
            echo'<div class="alert alert-danger alert-dismissible fade show" role="alert">
            <p>Data Succesfully Edited!</p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
             </div>
             ';
        }else{
            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
            <p>Data Failed to Edit</p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>';
        }
    }
}


//View Table
function viewTable(){
    $view = new view;
    $view->viewData();
    $view->viewCompletedData();
}

//Listener
function listener(){
    insertTodo();
    deleteTodo();
    editTodo();
}

?>